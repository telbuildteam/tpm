#!/usr/bin/python3
import urllib3
import argparse
import sys, os
from bs4 import BeautifulSoup

def get_file(url):
    os.system("wget " + url)

def get_package(name):
    pass

def update_lists():
    with open("tpm-sources.list", "r") as sources:
        with open("tpm.list", "w") as file:
            for source in sources:
                conn = urllib3.PoolManager()
                site = conn.request('GET', 'https://ftp.gnu.org/gnu')
                parser = BeautifulSoup(site.data.decode('utf-8'), 'html.parser')
                for link in parser.find_all('a'):
                    if link.get('href') is not None and \
                            link.get('href').endswith("/"):
                        print(link.get('href'))
                        file.write(source.rstrip() + '/' + link.get('href') + "\n")

def get_from_github(user_project):
    conn = urllib3.PoolManager()
    repo = conn.request('GET', 'https://github.com/'+user_project+'/archive/master.zip', preload_content=False)
    with open(user_project.split("/")[-1]+".zip", 'wb') as out:
        while True:
            data = repo.read(32)
            if not data:
                break
            out.write(data)
    repo.release_conn()

def search_github(query):
    repos = list()
    conn = urllib3.PoolManager()
    srch = conn.request('GET', 'https://github.com/search?q='+query)
    parser = BeautifulSoup(srch.data.decode('utf-8'), 'html.parser')
    for link in parser.find_all('a'):
        print(link.get('href'))
        if link.get('href') is not None and \
                not link.get('href').startswith("http") and \
                not link.get('href').startswith("/topics") and \
                not link.get('href').startswith("/search?") and \
                not "issues?" in link.get('href') and \
                len(link.get('href').split("/")) < 5:
            repos.append(link.get('href').lstrip("/"))
    repos = repos[10:]
    print(repos)
    return repos


if __name__ == "__main__":
    update_lists()
    search_github("pylint")
